﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulación_de_Autoclave_Castle
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            pbxFrontInside.Controls.Add(pbxFrontSide);
            pbxFrontSide.Location = new Point(0, 0);
        }

        private void btnSubir_Click(object sender, EventArgs e)
        {
            tmrSubir.Enabled = true;
            tmrBajar.Enabled = false;
        }

        private void btnBajar_Click(object sender, EventArgs e)
        {
            tmrBajar.Enabled = true;
            tmrSubir.Enabled = false;
        }

        private void tmrSubir_Tick(object sender, EventArgs e)
        {
            if (panelPuerta.Height <= 160)
            {
                //panelPuerta.Height = panelPuerta.Height + 5;
                //panelPuerta.Top = panelPuerta.Top - 5;
                panelPuerta.Height += 5;
                panelPuerta.Top -= 5;
            }
            else
            {
                tmrSubir.Enabled = false;
            }
        }

        private void tmrBajar_Tick(object sender, EventArgs e)
        {
            if (panelPuerta.Height >= 5)
            {
                panelPuerta.Height -= 5;
                panelPuerta.Top += 5;
            }
            else
            {
                tmrBajar.Enabled = false;
            }
        }
    }
}
